# pgt-api-server

Here's how to boostrap a working pgt-api-server:

## Install ansible on the machine

```sh
$ apt-get update && apt-get install -y ansible
```

## Create the provision file

This file is used to indicate which hosts will be provisioned

```sh
$ echo -e '[pgt-api-server]\n127.0.0.1' > .inventory
```

## Run ansible-playbook

```sh
$ ansible-playbook -i .inventory pgt-api-server.yaml
```

## Indicate SALSA_TOKEN env variable

(Note: this must be run on the host)

```sh
# systemctl edit pgt-api-server
```

And indicate the environment variable like this:

```
[Service]
Environment="SALSA_TOKEN=<redacted>"
```

Afterwards, you'll need to restart the service:

```sh
# systemctl daemon-reload
# systemctl restart pgt-api-server
```

## Enable the Letsencrypt certification generation

(Note: this must be run on the host)

```
# certbot --nginx -d pgt-api-server.debian.net
```

And follows the instructions

## Make the certificate auto-renew

(Note: this must be run on the host)

```
# crontab -e
```

And install the following crontab:

```
0 12 * * * /usr/bin/certbot renew --quiet
```

Enjoy :)
